import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An iterator to loop through points that are generated from an array of
 * random numbers.
 *  
 * @author Carmen Grantham
 *
 */
public class PointsIterator implements Iterator<Point> {
    private int position = 0;
    private double[] randomNumbers;
    
    /**
     * Create new object with array of random number 
     * @param numbers The random numbers to use in iterator
     */
    public PointsIterator(double[] numbers) {
        this.randomNumbers = numbers;
    }
    
    /**
     * Determine if there is another point to access.
     */
    public boolean hasNext() {
        return position < randomNumbers.length;
    }

    /**
     * Get the next Point to use.
     * @return The next point
     */
    public Point next() {
        if (hasNext()) {
            // Set x of the point to value at randomNumbers[position]
            Point point = new Point();
            point.x = randomNumbers[position];
            
            // Set y to be either the next value in randomNumbers array,
            // or if at end of array use value at position 0.
            if (position == randomNumbers.length - 1) {
                point.y = randomNumbers[0];
            } else {
                point.y = randomNumbers[position+1];
            }
            position++;
            
            return point;
        }
        throw new NoSuchElementException();
    }
    
    /**
     * @throws UnsupportedOperationException
     */
    public void remove() {
        // do nothing
        throw new UnsupportedOperationException();
    }
}
