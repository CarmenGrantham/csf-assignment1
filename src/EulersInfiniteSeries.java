/**
 * Program to use Euler's infinite series to calculate PI.
 * Formula is (^ indicates to the power of)
 *     PI^2/6 = 1/1^2 + 1/2^2 + 1/3^2 + ....
 * @author Carmen Grantham
 *
 */
public class EulersInfiniteSeries {

    /**
     * Use Euler's infinite series formula to calculate PI.
     * @param terms The number of terms to use in the formula
     * @return Result of the infinite series formula
     */
    public double calculate(int terms) {
        double result = 0;
        
        // For the number of terms add to the result 1/i^2
        // starting at 1
        for (int i = 1; i <= terms; i++) {
            result = result + (1 / Math.pow(i, 2)); 
        }
        
        // Multiply result by 6 to get PI^2
        result *= 6;
        
        // Square root result to get PI
        result = Math.sqrt(result);
        return result;
    }
    
    /**
     * Calculate the number of terms required to get 6 decimal point accuracy
     * of PI. 
     * @param startingTerms The number of terms to start with
     * @return The number of terms to match PI to 6 decimal places
     */
    public int calculateTo6DecimalPlaces(int startingTerms) {
        int terms = startingTerms;
        int multiplier = 1000000;
        
        // Multiply PI by 1,000,000 so that we can work with whole numbers
        int PI = (int)(Math.PI * multiplier);
        
        double result = 0;

        // Keep looping until the result matches PI to 6 decimal places
        while (PI != (int)(result * multiplier)) {
            result = calculate(terms);
            terms++;
        }
        
        // Return the number of terms to match PI to 6 decimal places
        return terms;
    }
    
    public static void main(String[] args) {
        System.out.println("Starting Euler's infinite series");
        System.out.printf("PI is %.14f\n\n", Math.PI);
        
        EulersInfiniteSeries series = new EulersInfiniteSeries();
        // Run the Eulers series with various terms to see how it compares
        // to PI to 6 decimal places
        int[] terms = new int[] {100, 1000, 10000, 100000, 1000000, 10000000, 100000000};
        for (int term : terms) {
            double result = series.calculate(term);
            System.out.printf("The result of using %,d terms is %.14f\n", term, result);
        }
        
        // Find exactly how many terms are needed to match PI to 6 decimal places
        // Using a staring term of 1,000,000 as we know that is correct up to 5 decimal places
        // and that by 10,000,000 it's correct to 6 places
        //int startingTerms = 1461000;
        int startingTerms = 1000000;
        int termsTo6DecimalPlaces = series.calculateTo6DecimalPlaces(startingTerms);
        System.out.printf("To get to 6 decimal places needed to use %d terms\n", termsTo6DecimalPlaces);
    }
}
