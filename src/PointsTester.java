
/**
 * Test that the points generated in MonteCarloSimulator are evenly spread
 * over unit square.
 * 
 * @author Carmen Grantham
 *
 */
public class PointsTester {

    /**
     * Display summary of the location of points with number of points within
     * - top section: x > y and x > 1 - y
     * - bottom section: x > y and x < 1 - y
     * - left section: x < y and x < 1 - y
     * - right section x < y and x > 1 - y 
     * @param points
     */
    private static void testPoints(MonteCarloSimulator simulator) {
        int leftSection = 0;
        int rightSection = 0;
        int topSection = 0;
        int bottomSection = 0;
        

        PointsIterator pointsIterator = new PointsIterator(simulator.randomNumbers);
        // Loop through all the points
        while (pointsIterator.hasNext()) {
            Point point = pointsIterator.next();
            double x = point.x;
            double y = point.y;
            
            // Add position of point to relevant section
            if (x < y) {
                if (x < 1 - y) {
                    leftSection++;
                } else {
                    rightSection++;
                }
            } else {
                if (x > 1 - y) {
                    topSection++;
                } else {
                    bottomSection++;
                }
            }
        }
        
        // Print out the results
        System.out.printf("Number of points in top section %,d\n", topSection);
        System.out.printf("Number of points in bottom section %,d\n", bottomSection);
        System.out.printf("Number of points in left section %,d\n", leftSection);
        System.out.printf("Number of points in right section %,d\n", rightSection);
        System.out.printf("Number of points in total %,d\n", topSection + bottomSection + leftSection + rightSection);
    }
    
    
    public static void main(String[] args) {
        // Run MonteCarlo Simulator for 1,000,000 points 
        int size = 1000000;
        System.out.printf("Starting Points Tester with %,d random numbers\n\n", size);
        
        MonteCarloSimulator simulator = new MonteCarloSimulator(size);
        testPoints(simulator);
        
        System.out.println("\nFinished Points Tester\n");
    }
}
