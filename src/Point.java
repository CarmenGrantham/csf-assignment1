
/**
 * Represent a point on a graph, which has an x and a y coordinate.
 * 
 * @author Carmen Grantham.
 *
 */
public class Point {

    public double x;
    public double y;
}
