/**
 * Program to use the Leibniz series to determine approximation of PI.
 * 
 * @author Carmen Grantham
 *
 */
public class LeibnizSeries {

    
    /**
     * Calculate the value of the Leibniz series using the number of terms 
     * provided.
     * @param terms The number of terms to use in Leibniz series
     * @return Result of Leibniz series
     */
    public double calculate(int terms) {
        double result = 0;
        double denominator = 1;
        
        // Use Leibniz series for the given number of terms using formula
        // 1 - (1/3) + (1/5) - (1/7)....
        for (int i = 0; i < terms; i++) {
            if (i % 2 == 0) {
                result = result + (1 / denominator); 
            } else {
                result = result - (1 / denominator);
            }
            denominator += 2;
        }
        
        // Multiply result by 4 to get equivalent of PI
        result *= 4;
        return result;
    }
    
    /**
     * Calculate the number of terms required to get 6 decimal point accuracy
     * of PI. 
     * @param startingTerms The number of terms to start with
     * @return The number of terms to match PI to 6 decimal places
     */
    public int calculateTo6DecimalPlaces(int startingTerms) {
        int terms = startingTerms;
        int multiplier = 1000000;
        
        // Multiply PI by 1,000,000 so that we can work with whole numbers
        int PI = (int)(Math.PI * multiplier);
        
        double result = 0;
        
        // Keep looping until the result matches PI to 6 decimal places
        while (PI != (int)(result * multiplier)) {
            result = calculate(terms);
            terms++;
        }
        
        // Return number of terms required.
        return terms;
    }
    
    public static void main(String[] args) {
        System.out.println("Starting Leibniz series");
        System.out.printf("PI is %.14f\n\n", Math.PI);
        
        // Run the Leibniz series with various terms to see how it compares
        // to PI to 6 decimal places
        LeibnizSeries series = new LeibnizSeries();
        int[] terms = new int[] {100, 1000, 10000, 100000, 1000000, 10000000};
        for (int term : terms) {
            double result = series.calculate(term);
            System.out.printf("The result of using %,d terms is %.14f\n", term, result);
        }            
     

        // Find exactly how many terms are needed to match PI to 6 decimal places
        // Using a staring term of 1,000,000 as we know that is correct up to 5 decimal places
        // and that by 10,000,000 it's correct to 6 places
        int startingTerms = 1455000;
        int termsTo6DecimalPlaces = series.calculateTo6DecimalPlaces(startingTerms);
        System.out.printf("To get to 6 decimal places needed to use %d terms\n", termsTo6DecimalPlaces);
        

        System.out.println("\nFinished Leibniz series");
    }
}
