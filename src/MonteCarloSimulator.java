

/**
 * Program to test formula used by Monte Carlo Simulator to determine area
 * of a circle.
 *    
 * @author Carmen Grantham
 *
 */
public class MonteCarloSimulator {

    public double[] randomNumbers;
    
    /**
     * Create a new object specifying how many random numbers to generate.
     * 
     * @param numbersToGenerate The number of random numbers to create.
     */
    public MonteCarloSimulator(int numbersToGenerate) {
        randomNumbers = RandomNumberGenerator.create(numbersToGenerate);
    }
    
    
    /**
     * Iterate through the random numbers creating points within an area and 
     * print the percentage that fall within the circle. 
     */
    private void processRandomNumbers() {
        int totalWithinCircle = 0;
        int pointsExamined = 0;
        
        PointsIterator pointsIterator = new PointsIterator(randomNumbers);
        
        // Loop through all the generated points
        while (pointsIterator.hasNext()) {
            Point point = pointsIterator.next();
            
            if (isWithinCircle(point)) {
                totalWithinCircle++;
            }
            pointsExamined++;
        }
        
        // Calculate the number of points within circle as a percentage.
        double percentage = (double) (4 * totalWithinCircle) / pointsExamined;
        
        System.out.printf("Percentage of points within circle: %.10f (PI is %.10f)\n",  percentage, Math.PI );
    }
    
    /**
     * Determine if a point (with an x and y coordinate) is within a circle
     * @param point The point to review
     * @return True if point is within circle.
     */
    private boolean isWithinCircle(Point point) {
        return isWithinCircle(point.x, point.y);
    }
    
    /**
     * Determine if x and y coordinates are within a circle
     * @param x The x coordinate
     * @param y The y coordinate
     * @return True if the x and y coordinates are within the circle.
     */
    private boolean isWithinCircle(double x, double y) {
        return (x * x) + (y * y) < 1;
    }
    
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        
        //int size = 1000000;      // 1 million points
        //int size = 10000000;     // 10 million points
        int size = 100000000;     // 100 million points
        System.out.printf("Starting Monte Carlo Simulator with %,d random numbers\n", size);
        
        // Execute the processing of random numbers a number of times to get a
        // realistic understanding of variations
        for (int i = 0; i < 10; i++ ) {
            MonteCarloSimulator simulator = new MonteCarloSimulator(size);
            simulator.processRandomNumbers();
        }
        
        // Finished simulator, print out how long it took to execute
        System.out.printf("Finished Monte Carlo Simulator in %f seconds\n\n\n", (double) (System.currentTimeMillis() - startTime) / 1000);
    }
    
}
