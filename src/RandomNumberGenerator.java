
/**
 * Program that generates a series of random numbers.
 * 
 * @author Carmen Grantham
 *
 */
public class RandomNumberGenerator {

    /**
     * Create a double array of random numbers, where the number of entries
     * is determined by the size parameter.
     * 
     * All random numbers will be greater than or equal to 0.0 and less than 1.0.
     * 
     * @param size The number of random numbers to create
     * @return Double array of random numbers.
     */
    public static double[] create(int size) {
        double[] randomNumbers = new double[size];
        for (int i = 0; i < randomNumbers.length; i++) {
            randomNumbers[i]= Math.random();
        }
        return randomNumbers;
    }
}
